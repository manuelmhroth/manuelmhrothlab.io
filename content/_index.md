---
title: Home
omit_header_text: true
description: "Home"
type: page
---

![Picture taken at GOSATCOM 2023 conference.](/images/gosatcom_2023.png "Probably talking about satellites.")

Manuel M. H. Roth received M.Sc. degrees in electrical engineering and information technologies from the Karlsruhe Institute of Technology and the Grenoble Institute of Technology in 2018 in the context of an international double degree program. He is currently pursuing his **Ph.D. degree** at the chair of signal processing at the **University of the Bundeswehr Munich**, Germany. His supervisor is Univ.-Prof. Dr.-Ing. Andreas Knopp.

Since 2018, he has been a Research Assistant with the Satellite Networks department of the **Institute of Communications and Navigation of the German Aerospace Center (DLR)**, Oberpfaffenhofen, Germany. Since 2021, he is also a member of the Munich Aerospace research group “Machine Learning for Network Management and Resource Allocation in Future Satellite Systems”. His research interests include satellite communication protocols, with a focus on **routing in satellite constellation networks**, constellation design, resource management and **reinforcement learning** techniques.

{{< include file="/publications/index.md" >}}
