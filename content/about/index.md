---
title: About
description: About me
omit_header_text: true
type: page
menu: main
weight: 2
---

![Picture taken at VDE ITE Expert Commitee 2024.](/images/vde_itg_2024.png "Talking about routing & network management optimization in satellite constellation networks.")

Manuel M. H. Roth received M.Sc. degrees in electrical engineering and information technologies from the Karlsruhe Institute of Technology and the Grenoble Institute of Technology in 2018 in the context of an international double degree program. He is currently pursuing his Ph.D. degree at the chair of signal processing at the University of the Bundeswehr Munich, Germany. His supervisor is Univ.-Prof. Dr.-Ing. Andreas Knopp.

Since 2018, he has been a Research Assistant with the Satellite Networks department of the Institute of Communications and Navigation of the German Aerospace Center (DLR), Oberpfaffenhofen, Germany. Since 2021, he is also a member of the Munich Aerospace research group “Machine learning for network management and resource allocation in future satellite systems”. His research interests include satellite communication protocols, with a focus on routing in satellite constellation networks, constellation design, resource management and reinforcement learning techniques.

### Employment
- `[2018-now]` **Research associate -- German Aerospace Center (DLR)**

    Institute of Communications and Navigation, Satellite Networks department

### Education
- `[2020-now]` **Dr.-Ing. -- Bundeswehr University Munich**

    Information Technology, Chair of Signal Processing

- `[2015-2018]` **M.Sc. -- Karlsruhe Institute of Technology**

    Electrical Engineering and Information Technology

- `[2015-2018]` **M.Sc -- Grenoble Institute of Technology**

    Computer Science -- Internet, Services and Connected Systems

### Miscellaneous Experience
- `[2021-now]` **Member of Munich Aerospace research group**

    Machine learning for network management and resource allocation in future satellite systems

- `[2020]` **Deep Learning Specification**

    Coursera certification consisting of five courses

- `[2017]` **Rohde \& Schwarz Engineering Competition World League**

    Enhancing a software-based DVB-T2-Coder based on the GNU Radio project
