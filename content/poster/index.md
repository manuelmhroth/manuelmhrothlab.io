---
title: AINet Poster
description: KuVS - AI in Networks Summer School 22 poster
omit_header_text: true
type: page
menu: main
weight: 4
---

### KuVS - AI in Networking Summer School 2022

![Research poster for the KuVS - AI in Networking Summer School 2022.](/images/ainet_poster.png " ")

### Sources:

 1. R. E. Ali, B. Erman, E. Baştuğ, and B. Cilli, “Hierarchical Deep Double Q-Routing,” arXiv, arXiv:1910.04041, Mar. 2020. doi: [10.48550/arXiv.1910.04041](https://doi.org/10.48550/arXiv.1910.04041)
 2. H. van Hasselt, A. Guez, and D. Silver, “Deep Reinforcement Learning with Double Q-learning,” arXiv, arXiv:1509.06461, Dec. 2015. doi: [10.48550/arXiv.1509.06461](https://doi.org/10.48550/arXiv.1509.06461)
 3. A. Bitaillou, B. Parrein, and G. Andrieux, “Q-routing: From the Algorithm to the Routing Protocol,” in Machine Learning for Networking, Cham, 2020, pp. 58–69. doi: [10.1007/978-3-030-45778-5_5](10.1007/978-3-030-45778-5_5)
 4. S. Kim, J. Son, A. Talukder, and C. S. Hong, “Congestion prevention mechanism based on Q-leaning for efficient routing in SDN,” in 2016 International Conference on Information Networking (ICOIN), Jan. 2016, pp. 124–128. doi: [10.1109/ICOIN.2016.7427100](https://doi.org/10.1109/ICOIN.2016.7427100)
 5. P. Gawłowicz and A. Zubow, “ns-3 meets OpenAI Gym: The Playground for Machine Learning in Networking Research,” in Proceedings of the 22nd International ACM Conference on Modeling, Analysis and Simulation of Wireless and Mobile Systems  - MSWIM ’19, Miami Beach, FL, USA, 2019, pp. 113–120. doi: [10.1145/3345768.3355908](10.1145/3345768.3355908)
 6. M. Handley, “Delay is Not an Option: Low Latency Routing in Space,” in Proceedings of the 17th ACM Workshop on Hot Topics in Networks, New York, NY, USA, Nov. 2018, pp. 85–91. doi: [10.1145/3286062.3286075](https://doi.org/10.1145/3286062.3286075)
 7. Met Office, “Cartopy: a cartographic python library with a Matplotlib interface,” Exeter, Devon, manual, 2015 2010. [Online]. Available: https://scitools.org.uk/cartopy
 8. A. Papa, T. de Cola, P. Vizarreta, M. He, C. Mas-Machuca, and W. Kellerer, “Design and Evaluation of Reconfigurable SDN LEO Constellations,” IEEE Transactions on Network and Service Management, vol. 17, no. 3, pp. 1432–1445, Sep. 2020, doi: [10.1109/TNSM.2020.2993400](https://doi.org/10.1109/TNSM.2020.2993400)
 9. M. M. H. Roth and H. Bischl, “Adaptives Routing-Protokoll für Satellitenkonstellationen mit ISLs,” presented at the Nationale Konferenz Satellitenkommunikation 2021, 2021. Accessed: Jan. 24, 2022. [Online]. Available: https://elib.dlr.de/cgi/users/home?screen=EPrint%3A%3AView&eprintid=144697
 10. M. M. H. Roth, H. Brandt, and H. Bischl, “Implementation of a geographical routing scheme for low Earth orbiting satellite constellations using intersatellite links,” International Journal of Satellite Communications and Networking, vol. 39, no. 1, pp. 92–107, 2021, doi: [10.1002/sat.1361](https://doi.org/10.1002/sat.1361)
 11. F. Bannour, S. Souihi, and A. Mellouk, “Distributed SDN Control: Survey, Taxonomy, and Challenges,” IEEE Communications Surveys Tutorials, vol. 20, no. 1, pp. 333–354, 2018, doi: [10.1109/COMST.2017.2782482](https://doi.org/10.1109/COMST.2017.2782482)
 12. J. Xie et al., “A Survey of Machine Learning Techniques Applied to Software Defined Networking (SDN): Research Issues and Challenges,” IEEE Communications Surveys Tutorials, vol. 21, no. 1, pp. 393–430, 2019, doi: [10.1109/COMST.2018.2866942](https://doi.org/10.1109/COMST.2018.2866942)
 13. C. Zhang, P. Patras, and H. Haddadi, “Deep Learning in Mobile and Wireless Networking: A Survey,” IEEE Communications Surveys Tutorials, vol. 21, no. 3, pp. 2224–2287, 2019, doi: [10.1109/COMST.2019.2904897](https://doi.org/10.1109/COMST.2019.2904897)

