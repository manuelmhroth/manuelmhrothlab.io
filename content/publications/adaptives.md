---
weight: 3
title: Adaptives Routing-Protokoll für Satellitenkonstellationen mit ISLs
date: 2021-05-04
omit_header_text: true
description: "Conference presentation"
menu: publications
---

A conference presentation in German about routing in satellite constellation networks for the "Nationale Konferenz Satellitenkommunikation 2021".

### Abstract:

Satellitenkonstellationen in erdnahen Umlaufbahnen, die über Inter-Satelliten-Verbindungen verfügen, stellen höchst dynamische Netzwerkumgebungen dar. Bei Verwendung solcher Systeme für den breitbandigen Internet-Zugang entsteht ein ungleichmäßig verteilter Datenverkehr, welcher zu lokalen Kapazitäts-Engpässen in einzelnen Satelliten führen kann. Zudem müssen verschiedene Quality of Service Klassen unterstützt werden. Als Lösung für diese Problematiken wurden am DLR Institut für Kommunikation und Navigation adaptive Routing- und Netzwerkmanagement Protokolle konzipiert und in Systemsimulationen analysiert. Das erste entwickelte Protokoll basiert auf geographischem Routing. Pakete werden mit einem geographischen Indikator versehen, damit sie unabhängig von der Netzwerktopologie an ihr Ziel gelangen. Diese Entkopplung von der Satellitenbewegung führt zu einer drastischen Verringerung des benötigten Signalisierungsaufwands. Allerdings sind solche Verfahren relativ unflexibel und erlauben nur einen begrenzten Lastausgleich. Um auf eine ungleiche Netzwerklast besser reagieren zu können und den Quality of Service Anforderungen besser zu entsprechen, wurden Ansätze basierend auf Software Defined Networking konzipiert. Damit können virtuelle Netzwerke sowie Forwarding-Regeln in den Switches der Satelliten flexibel konfiguriert werden. Zudem werden dadurch Erweiterungen wie die Integration von 5G/6G oder die Verwendung maschinellen Lernens vereinfacht. Die vorgeschlagenen Protokolle sind vorteilhaft hinsichtlich Latenz, Verlustrate und Signalisierungsaufwand. Dies ermöglicht einen effizienteren Datenverkehr innerhalb von Satellitenkonstellationen.
