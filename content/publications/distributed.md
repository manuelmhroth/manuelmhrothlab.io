---
weight: 3
title: Distributed SDN-based Load-balanced Routing for Low Earth Orbit Satellite Constellation Networks
date: 2022-09-22
omit_header_text: true
description: "Preprint"
menu: publications
---

Preprint of an accepted conference paper: [article link](https://arxiv.org/abs/2209.05984)

### Abstract:

With the current trend towards low Earth orbit mega-constellations with inter-satellite links, efficient routing in such highly dynamic space-borne networks is becoming increasingly important. Due to the distinct network topology, specifically tailored solutions are required. Firstly, the relative movement of the constellation causes frequent handover events between the satellites and the terminals on ground. Furthermore, unevenly distributed traffic demands lead to geographical hot spots. The physical size of the network also implies significant propagation delays. Therefore, monitoring the dynamic topology changes and link loads on a network-wide basis for routing purposes is typically impractical with massive signaling overhead. To address these issues, we propose a distributed load-balanced routing scheme based on Software Defined Networking. The approach divides the large-scale network into sub-sections, called clusters. In order to minimize signaling overhead, packets are forwarded between these clusters according to geographical heuristics. Within each cluster active Quality of Service-aware load-balancing is applied. The responsible on-board network controller forwards routing instructions based on the network state information in its cluster. We also analyze specific design choices for the clusters and the interfaces between them. The protocol has been implemented in a system-level simulator and compared to a source-routed benchmark solution.
