---
weight: 3
title: Implementation of a geographical routing scheme for low Earth orbiting satellite constellations using intersatellite links
date: 2020-06-26
omit_header_text: true
description: "Journal paper"
menu: publications
---

A paper illustrating the design and implementation of a geogrpahical routing scheme for LEO satellite constellations: [article link](https://onlinelibrary.wiley.com/doi/10.1002/sat.1361)


### Abstract:
A geographical routing scheme for LEO satellite constellations with intersatellite links is proposed, implemented, and analyzed for two Walker Star constellations. The approach targets reliable transmissions with low latency and high data rates even in scenarios with significant traffic load. An Iridium‐like constellation and a megaconstellation are investigated and compared regarding the address resolution procedures, the average end‐to‐end transmission delay, and the dropping and rerouting rates using a system level simulator.
