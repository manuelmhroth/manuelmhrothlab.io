---
title: Publications & Talks
description: Publications & Talks
omit_header_text: true
type: page
menu: main
weight: 3
---

### Publications
- Roth, M. M. H., Hegde, A., Delamotte, T., & Knopp, A. (2024, September). **Shaping Rewards, Shaping Routes: On Multi-Agent Deep Q-Networks for Routing in Satellite Constellation Networks**. In *Proceedings of SPAICE 2024: The first joint European Space Agency SPAICE Conference / IAA Conference on AI in and for Space* (pp. 100-104). doi: https://doi.org/10.5281/zenodo.13889941
- Roth, M. M. H. (2023, October). **Analyzing Source-Routed Approaches for Low Earth Orbit Satellite Constellation Networks**. In *Proceedings of the 1st ACM Workshop on LEO Networking and Communication* (pp. 43-48). doi: https://doi.org/10.1145/3614204.3616109
- Roth, M. M. H., Brandt, H., & Bischl, H. (2022, September). **Distributed sdn-based load-balanced routing for low earth orbit satellite constellation networks**. In *2022 11th Advanced Satellite Multimedia Systems Conference and the 17th Signal Processing for Space Communications Workshop (ASMS/SPSC)* (pp. 1-8). IEEE. doi: https://doi.org/10.1109/ASMS/SPSC55670.2022.9914690
- Roth, M. M. H., Brandt, H., & Bischl, H. (2021). **Implementation of a geographical routing scheme for low Earth orbiting satellite constellations using intersatellite links**. *International Journal of Satellite Communications and Networking*, 39(1), 92-107. doi: https://doi.org/10.1002/sat.1361

### Reports
- S. Kota, G. Giambene, M. Abbak, M. Abdelsadek, M.-S. Alouini, M. Amay, S. Babu, J. Bas, V. Baños González, C. Carballo González, P. Cassarà, S. Chaudhari, D. Dalai, T. Darwish, T. de Cola, T. Delamotte, A. Dutta, A. Dwivedi, H. ElBakoury, M. Enright, M. Giordani, A. Gotta, E. Hammad, T. Khattab, A. Knopp, G. Karabulut Kurt, T. Li, P. G. Madoery, B. S. Manoj, J.-D. Medjo Me Biomo, P. Pillai, P. Rawat, M. M. H. Roth, P. Saxena, P. Scanlan, A. Sharma, M. S. Solaija, R. Sperber, Z. Sun, D. Tarchi, N. Varshney, S. Verma, R. Wang, S. Watts, H. Yanikomeroglu, K. Zhao, and L. Zhao, "**Satellite**," *IEEE International Network Generations Roadmap - 2024 Edition, 2024 IEEE Future Networks World Forum (FNWF)*, IEEE, 2024.

### Invited & Conference Talks
- "**Shaping Rewards, Shaping Routes: On Multi-Agent Deep Q-Networks for Routing in Satellite Constellation Networks**". Presented at *1st Joint European Space Agency / IAA Conference on AI in and for Space (SPAICE2024)*, ESA European Centre for Space Applications and Telecommunications (ECSAT), Harwell, UK. September 17-19, 2024.
- "**Routing in Satellite Constellation Networks – Rule-based vs. Machine Learning Approaches**". Presented at *The Expert Committee HF 2 »Radio Systems« of the VDE ITG: Mobile Communications Go Space - Opportunities for NTN in 6G*, University of the Bundeswehr Munich, Neubiberg. June 17-18, 2024.
- "**Analyzing Source-Routed Approaches for Low Earth Orbit Satellite Constellation Networks**". Presented at *LEO-NET '23: 1st ACM Workshop on LEO Networking and Communication*, Madrid. October 6, 2023.
- "**Routing & Network Management Optimization in LEO Satellite Constellation Networks using Machine Learning Techniques**. Invited talk presented at *IEEE International Network Generations Roadmap (INGR) Satellite Working Group*, online. September 28, 2023.
- "**Maschinelles Lernen für Netzwerk-Management in Mega-Konstellationen**". Presented at *GOSATCOM – Nationale Konferenz für behördliche Satellitenkommunikation*, University of the Bundeswehr Munich, Neubiberg. March 27-29, 2023.
- "**Enabling Effective Multi-Link Data Distribution in NTN-based 6G Networks**". With T. De Cola. Presented at *26th Workshop on Smart Antennas and 13th Conference on Systems, Communications, and Coding (WSA/SCC)*, Brunswick. February 27 - March 3, 2023.
- "**On tailoring Deep Learning techniques: Deep Q-Routing**". Presented at *Zugspitze Workshop on Communications 2023*, Environmental Research Station Schneefernerhaus, Zugspitze. January 23-26, 2023.
- "**Machine Learning for Network Management and Resource Allocation in Future Satellite Systems**". With D. Weinzierl and T. Delamotte. Presented at *4th Global Aerospace Summit of Munich Aerospace*, Herrsching. September 19-21, 2022.
- "**Distributed SDN-based Load-balanced Routing for Low Earth Orbit Satellite Constellation Networks**". Presented at *11th Advanced Satellite Multimedia Systems Conference and 17th Signal Processing for Space Communications Workshop (ASMS/SPSC)*, Joanneum Research, Graz. September 6-8, 2022.
- "**On-Board Routing for LEO High Throughput Satellites**. Invited lecture presented at *SatNEx Summer School*, Joanneum Research, Graz. September 4-5, 2022.
- "**Adaptives Routing-Protokoll für Satellitenkonstellationen mit ISLs**". Presented virtually at *Nationale Konferenz Satellitenkommunikation*, online. May 18, 2021.

### Further Events
- *INCOMING - DLR PhD School*, German Aerospace Center, Oberpfaffenhofen. 2023.
- *KuVS - AI in Networking Summer School*, Günzburg. 2022.
- *ESA Satellite Constellation Networking Tools Workshop*, online. 2022.
- *10th Advanced Satellite Multimedia Systems Conference and 16th Signal Processing for Space Communications Workshop (ASMS/SPSC)*, online. 2020.
- *GOSATCOM – Nationale Konferenz für behördliche Satellitenkommunikation*, University of the Bundeswehr Munich, Neubiberg. 2019.
- *6th Training School on Machine and Deep Learning Techniques for (Beyond) 5G Wireless Communication Systems*, Centre Tecnòlogic de Telecomunicacions de Catalunya (CTTC), Barcelona. 2019.
- *9th Advanced Satellite Multimedia Systems Conference and 15th Signal Processing for Space Communications Workshop (ASMS/SPSC)*, Berlin. 2018.

### Reviewing Activities
More than 50 peer reviews for numerous journals and conferences, including:
- IEEE Transactions on Aerospace and Electronic Systems
- IEEE Transactions on Vehicular Technology
- International Journal of Satellite Communications and Networking
- IEEE Communication Letters
- IEEE Systems Journal
- IEEE Wireless Communication Letters
- IEEE International Conference on Communications
- IEEE Global Communications Conference
- Advanced Satellite Multimedia Systems Conference
