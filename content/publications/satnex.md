---
weight: 3
title: On-Board Routing for LEO High Throughput Satellites (SatNEx School)
date: 2022-09-05
omit_header_text: true
description: "Lecture"
menu: publications
---

Invited talk for the SatNEx summer school 2022.

### Abstract:
In this lecture, we focus on networking aspects of low Earth orbit mega-constellations with inter-satellite links for broadband traffic. For such highly dynamic space-borne networks, efficient routing is becoming increasingly important. Therefore, we elaborate on the distinct network characteristics and topologies, and the specifically tailored routing solutions. Furthermore, corresponding on-board resource as well as network management strategies are discussed. Key concepts of modern network design are investigated, such as distributed networks, multipath routing, load-balancing, QoS-awareness, Software Defined Networking, and multicasting.
